# Model 
This documents model development and configurations. See ./model.ipynb for code.

# Data Set: 
See ./training_set.csv 
<br>
See ./validation_set.csv
<br>
See ./testing_set.csv
<br>

The model utilizes three different datasets. 
1. dataset.json: set consists of 18,251 questions
2. dataset2.json: set consists of 17,571 questions
3. questions.csv: set consist of 404,352 questions
<br>
Datset.json was used as the primary dataset and dataset2.json and questions.csv were used to supplement questions type that didn't appear as often as others ( i.e. long text ) to provide equal distribution of question types

# Data Preprocessing: 
BERT (Bidirectional Encoder Representations from Transformers) is used in order to pre-train the contextualized word embeddings. BERT applies a bidirectional training of the Transformers attention model. To use BERT, the model is utilizing Pytorch and  the Transformers Library by HuggingFace.

The dataset was converted into answer types depending on the type label given in the dataset and by the question structure (i.e. a "why" question is labeled as "long text" and a "how many" question is labeled as "numeric"). After converting, we ensured an equal distribution of sentiments. Three encoding requirements are needed to use BERT: 
1. Special tokens to delineate separate sentences
2. Introduce padding to ensure constant length of review
3. Create attention mask (pad and real tokens)
The pre-trained Bert Tokenizer model has a function that encodes the data ( tokenizer.encode_plus() )  

# Model Development: 
The following model was created using code from Venelin Valkov ( see references ). 

The model uses a neural network classifier based on the BERT model, a dropout layer ( to avoid overfitting), and a fully-connected layer. The BERT model utilizes fixed sequence length. Based on distribution of review lengths, the max length chosen is 160. A softmax function is used to predict the probabilities of each sentiment. Finally, to correct for weight decay, the AdamW optimizer is used. 

The fine-tuning parameters recommended by the BERT developers are as follows: 

Batch size: 16, 32 ( model used 16 ) 
Learning rate (Adam): 5e-5, 3e-5, 2e-5 (  model used 2e-5 ) 
Number of epochs: 2, 3, 4 ( model used 10 ) 
10 epoch was used because it produced the greatest training accuracy results. 
The model produced a testing set accuracy of 0.883248730964467
The confusion matrix is attached in model_confusion_matrix.png. 


# Model Deployment: 
The model is deployed using the Flask web framework. The Flask CORS library implements Cross Origin Resource Sharing (CORS) capabilities.  The user may input statements via a json file or text box. 