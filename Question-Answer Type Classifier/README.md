# Survey Question Answer Classifier
This service classifies questions by their answer types ( 'small text', 'small text- location','small text- person', 'long text', 'multiple choice', 'date and time', 'yes/no', 'numeric' ) and produces a distribution of answer types

# Configurations
```bash
[packages]
Flask 1.1.2
Flask-cors 3.0.8
Jinja2 2.11.2
Pytorch 1.5.1
Transformers 3.0.2
```

# Development
To start the application, run: 
```bash
python /src/app_multy.py
```

# Docker 
To run the docker image, run: 
```bash
docker run -d -p 5000:5000 registry.gitlab.com/stg-et/common-ai-services/question-answer-type
```

# Model Documentation
Model code is located in ./model.ipynb
<br>
[Model Documentation](model_doc.md)

# File Directories
The question answer type classifier outputs the distributions of answer types from all the statements as well as the answer type probabilities for each statement. 

1. ./src/app.py: python file to deploy model Postman json request
2. ./src/Best_model_state.bin : model was trained using google colab’s environment and downloaded to the project
4. ./src/templates/index.html: html page for user to input statement
5. ./src/templates/result.html: html page for resulting sentiment
6. ./src//static/styles.css: css for html appearance

# POST Request Types
For json output request: http://127.0.0.1:5000/question_analysis/json

For html output request: http://127.0.0.1:5000/question_analysis/text

# Example Request app_multy.py
```bash
curl --location --request POST 'http://127.0.0.1:5000/question_analysis/json' \
--data-raw '{ "statement": [
     "Why did the chicken cross the road?","What is the biggest country?", 
     "Is this true?", 
     "Who discovered bread?", 
     "Explain the history of the world", 
     "When is it due?", 
     "How many dogs do you have?", 
     "Is this the right person?"
     ] }'
```

# Input json file 
```bash
{ "statement": [
     "How good was the customer service?", 
     "Why did the chicken cross the road?", 
     "Social Security?",
     "What is the biggest country?", 
     "Is this true?", 
     "Where is this town?", 
     "How impactful was this pandemic?", 
     "When is it due?", 
     "How many dogs do you have?", 
     "Is this the right person?"
     ] }
```

# Output json file
```bash
{
  "Counts": {
    "date and time": 1,
    "long text": 2,
    "multiple choice": 1,
    "numeric": 2,
    "small text": 1,
    "small text- location": 1,
    "small text- person": 0,
    "yes/no": 2
  },
  "Predictions": [
    "multiple choice",
    "long text",
    "numeric",
    "small text",
    "yes/no",
    "small text- location",
    "long text",
    "date and time",
    "numeric",
    "yes/no"
  ],
  "Probabilities": [
    {
      "date and time": 3.622093936428428e-05,
      "long text": 3.519164602039382e-05,
      "multiple choice": 0.9997652173042297,
      "numeric": 3.0208304451662116e-05,
      "small text": 1.2683777640631888e-05,
      "small text- location": 4.103054743609391e-05,
      "small text- person": 6.410651258192956e-05,
      "yes/no": 1.5340392565121874e-05
    },
    {
      "date and time": 0.0001928567508002743,
      "long text": 0.9994358420372009,
      "multiple choice": 4.8894256906351075e-05,
      "numeric": 4.744605757878162e-05,
      "small text": 6.425465835491195e-05,
      "small text- location": 1.404558770445874e-05,
      "small text- person": 4.2501178540987894e-05,
      "yes/no": 0.00015414919471368194
    },
    {
      "date and time": 4.500095019466244e-05,
      "long text": 3.542234480846673e-05,
      "multiple choice": 3.001273944391869e-05,
      "numeric": 0.9996799230575562,
      "small text": 9.393289656145498e-05,
      "small text- location": 3.9142832974903286e-05,
      "small text- person": 2.7370597308618017e-05,
      "yes/no": 4.918693593936041e-05
    },
    {
      "date and time": 0.00018874485976994038,
      "long text": 0.00011965302110183984,
      "multiple choice": 0.00152574700769037,
      "numeric": 0.001419233507476747,
      "small text": 0.7225390672683716,
      "small text- location": 0.27035126090049744,
      "small text- person": 0.003741202875971794,
      "yes/no": 0.00011503374116728082
    },
    {
      "date and time": 8.219688606914133e-05,
      "long text": 5.352679727366194e-05,
      "multiple choice": 5.0842496420955285e-05,
      "numeric": 0.0001152573386207223,
      "small text": 0.0001222291321028024,
      "small text- location": 5.5304186389548704e-05,
      "small text- person": 6.23530286247842e-05,
      "yes/no": 0.999458372592926
    },
    {
      "date and time": 1.6973726815194823e-05,
      "long text": 1.592635817360133e-05,
      "multiple choice": 0.00016365176998078823,
      "numeric": 0.0002867571893148124,
      "small text": 0.0012278894428163767,
      "small text- location": 0.9977707862854004,
      "small text- person": 0.0004904246889054775,
      "yes/no": 2.763686643447727e-05
    },
    {
      "date and time": 0.00010749234206741676,
      "long text": 0.9929592609405518,
      "multiple choice": 0.005524895619601011,
      "numeric": 0.0007882565259933472,
      "small text": 6.140478944871575e-05,
      "small text- location": 4.3932934204349294e-05,
      "small text- person": 0.0001692319638095796,
      "yes/no": 0.00034552565193735063
    },
    {
      "date and time": 0.9990610480308533,
      "long text": 6.432466034311801e-05,
      "multiple choice": 8.180369331967086e-05,
      "numeric": 0.0002747353573795408,
      "small text": 0.0002368926943745464,
      "small text- location": 0.00010208157618762925,
      "small text- person": 0.00016487948596477509,
      "yes/no": 1.4213236681825947e-05
    },
    {
      "date and time": 0.00010840535105671734,
      "long text": 0.0004847953678108752,
      "multiple choice": 9.967083315132186e-05,
      "numeric": 0.9987589120864868,
      "small text": 5.575819159275852e-05,
      "small text- location": 4.585625356412493e-05,
      "small text- person": 5.1582504966063425e-05,
      "yes/no": 0.00039500658749602735
    },
    {
      "date and time": 0.0005134799866937101,
      "long text": 0.0033938195556402206,
      "multiple choice": 0.0010888040997087955,
      "numeric": 0.0006650408613495529,
      "small text": 0.07240951061248779,
      "small text- location": 0.0031720229890197515,
      "small text- person": 0.04421519115567207,
      "yes/no": 0.8745421171188354
    }
  ],
  "Sentances": [
    "How good was the customer service?",
    "Why did the chicken cross the road?",
    "Social Security?",
    "What is the biggest country?",
    "Is this true?",
    "Where is this town?",
    "How impactful was this pandemic?",
    "When is it due?",
    "How many dogs do you have?",
    "Is this the right person?"
  ]
}
```

# Postman Documentation
https://documenter.getpostman.com/view/11998870/T17Q44En


# References 
https://towardsdatascience.com/deploying-bert-using-kubernetes-6ddca23caec5

https://www.curiousily.com/posts/deploy-bert-for-sentiment-analysis-as-rest-api-using-pytorch-transformers-by-hugging-face-and-fastapi/

https://www.curiousily.com/posts/sentiment-analysis-with-bert-and-hugging-face-using-pytorch-and-python/

https://ai.googleblog.com/2018/11/open-sourcing-bert-state-of-art-pre.html

https://medium.com/@doedotdev/docker-flask-a-simple-tutorial-bbcb2f4110b5?fbclid=IwAR0fdSncZZgIlmnUcQREdesHUSmghT3ZLq6Rx1ciMPsxaHopD3n7_HlMxmo
