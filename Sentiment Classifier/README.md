# Sentiment Analysis Classifier using BERT
This service outputs sentiments ( positive, neutral, negative) given statements

# Configurations
```bash
[packages]
Flask 1.1.2
Flask-cors 3.0.8
Jinja2 2.11.2
Pytorch 1.5.1
Transformers 3.0.2
```

# Development
To start the application, run: 
```bash
python /src/app_multy.py
```

# Docker 
To run the docker image, run: 
```bash
docker run -d -p 5000:5000 registry.gitlab.com/stg-et/common-ai-services/sentiment-analysis
```

# Model Documentation
Model code is located in ./model.ipynb
<br>
[Model Documentation](model_doc.md)

# File Directories
The sentiment classifier outputs the distributions of sentiments from all the statements as well as the sentiment and probabilities for each statement. 

1. ./src/app.py: python file to deploy model with options for Postman json request, user type in request and user upload file request
2. ./src/Best_model_state.bin : model was trained using google colab’s environment and downloaded to the project
3. app.py: python file to deploy the model using flask
4. ./src/templates/index.html: html page for user to input statement
5. ./src/templates/result.html: html page for resulting sentiment
6. ./src//static/styles.css: css for html appearance
7. ./src/tests/app_multy_tests.py: python file to begin unittesting
8. ./docs/source/index.rst: rst file for documentation

# POST Request Types
For non-Postman request: http://127.0.0.1:5000/sentiment_analysis/

For Postman request: http://127.0.0.1:5000/sentiment_analysis/services/general/json

# Example Request app_multy.py
```bash
curl --location --request POST 'http://127.0.0.1:5000/sentiment_analysis/services/sentiments' \
--data-raw '{ "statement": [ "This is good. But the UI could have been better. Copy and Move feature also needed.", "This is bad. But the API could have been better. Copy and Move feature is must. ", "This is best. Copy and Move feature also needed." ] }'
```

# Input json file app_multy.py
```bash
{ "statement": [
     "This is good. But the UI could have been better. Copy and Move feature also needed.", 
     "This is bad. But the API could have been better. Copy and Move feature is must. ", 
     "This is best. Copy and Move feature also needed." 
     ] }
```

# Response json file
```bash
{
  "Counts": {
    "Negitive Count": 1,
    "Neutral Count": 2,
    "Positive Count": 0
  },
  "Predictions": [
    "neutral",
    "negative",
    "neutral"
  ],
  "Probabilities": [
    {
      "negative": 6.203648808877915e-05,
      "neutral": 0.9998505115509033,
      "positive": 8.752060966799036e-05
    },
    {
      "negative": 0.9997557997703552,
      "neutral": 0.00011095738591393456,
      "positive": 0.00013325289182830602
    },
    {
      "negative": 0.0001267982297576964,
      "neutral": 0.9997733235359192,
      "positive": 9.98378309304826e-05
    }
  ],
  "Sentances": [
    "This is good. But the UI could have been better. Copy and Move feature also needed.",
    "This is bad. But the API could have been better. Copy and Move feature is must. ",
    "This is best. Copy and Move feature also needed."
  ]
}
```

# Postman Documentation
https://documenter.getpostman.com/view/11998870/T17Q44En


# References 
https://towardsdatascience.com/deploying-bert-using-kubernetes-6ddca23caec5

https://www.curiousily.com/posts/deploy-bert-for-sentiment-analysis-as-rest-api-using-pytorch-transformers-by-hugging-face-and-fastapi/

https://www.curiousily.com/posts/sentiment-analysis-with-bert-and-hugging-face-using-pytorch-and-python/

https://ai.googleblog.com/2018/11/open-sourcing-bert-state-of-art-pre.html

https://medium.com/@doedotdev/docker-flask-a-simple-tutorial-bbcb2f4110b5?fbclid=IwAR0fdSncZZgIlmnUcQREdesHUSmghT3ZLq6Rx1ciMPsxaHopD3n7_HlMxmo
