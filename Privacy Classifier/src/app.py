
from flask import Flask,render_template,url_for,request,redirect,flash,jsonify,Response

import io

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from mpl_toolkits.mplot3d import Axes3D

from transformers import BertModel, BertTokenizer
import torch
import json

from flask_cors import CORS
from torch import nn, optim
from torch.utils.data import Dataset, DataLoader

import torch.nn.functional as F
import sys
from werkzeug.serving import run_simple
from werkzeug.middleware.dispatcher import DispatcherMiddleware

device = torch.device('cpu')

app = Flask(__name__,template_folder="templates")

# establish base route path
app.config['APPLICATION_ROOT'] = '/privacy-classifier'

#implement CORS
CORS(app)

PRE_TRAINED_MODEL_NAME = 'bert-base-cased'

tokenizer = BertTokenizer.from_pretrained(PRE_TRAINED_MODEL_NAME) #BERT tokenizer model 

file_information = []

#function to establish basic root path
def simple(env, resp):
    resp(b'200 OK', [(b'Content-Type', b'text/plain')])
    return [b'Privacy Classifier']

app.wsgi_app = DispatcherMiddleware(simple, {'/privacy-classifier': app.wsgi_app})

# classifer bert model
class QuestionClassifier(nn.Module):

  def __init__(self, n_classes):
    super(QuestionClassifier, self).__init__()
    self.bert = BertModel.from_pretrained(PRE_TRAINED_MODEL_NAME)
    self.drop = nn.Dropout(p=0.3)
    self.out = nn.Linear(self.bert.config.hidden_size, n_classes)

  def forward(self, input_ids, attention_mask):
    _, pooled_output = self.bert(
      input_ids=input_ids,
      attention_mask=attention_mask
    )
    output = self.drop(pooled_output)
    return self.out(output) 


"""A class that is used to store vital information
"""
class AnalysisInfo():
    global_question_type = []
    global_sentence_list = []
    global_probabilities_list = []
    global_count_list = []

    """Does not return anything. It keeps track of variables used globaly

    :returns: A index.html template
    """
    def set_globeal(self,question_type,sentence_list,probabilities_list,count_list):
        self.global_question_type = question_type
        self.global_sentence_list = sentence_list
        self.global_probabilities_list = probabilities_list
        self.global_count_list = count_list


"""It holds the information regarding sentances and their question_type
"""
information = AnalysisInfo()


@app.route('/')
def home():
    """Returns the index.html template with fields that would allow users to
    inpute a json file or a field where they can inpute their own sentances

    :returns: A index.html template
    """
    return render_template('index.html')

@app.route('/json',methods=['GET','POST'])
def remote_json():
    """User inputs json
    
    Returns a json output file of results
    """
    value = request.json
    return _predict(value['statement'],1)

@app.route('/text',methods=['GET','POST'])
def remote_text():
    """User inputs json
    
    Returns an html output file of results
    """
    value = request.json
    return _predict(value['statement'])

@app.route('/file',methods=['POST','GET'])
def file():
    """User input file
    
    Returns output file of results
    """
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file_information = request.files['file']
        file_information.seek(0)
        file_information = json.loads(file_information.read())
        print(file_information, file=sys.stderr)
        return _predict(file_information['statement'])


def _predict(argument,josnit=0):
    """Returns the index.html template with fields that would allow users to
    inpute a json file or a field where they can inpute their own sentances

    :param argument: the sentences read via the UI
    :param josnit: 0 return html template 1 retunr a json object 
    :returns: A result.html template or a json file
    """
    class_names = ['NonPrivate', 'Private']
    #load model
    model = QuestionClassifier(len(class_names))
    model.load_state_dict(torch.load('best_model_state.bin', map_location=torch.device('cpu')))
    model = model.to(device)

    #counters for statistics
    req = argument
    non_priv = 0
    priv = 0
    
    question_type = []
    sentence_list = []
    probabilities_list = []
    count_list = []
    
    for review in req:
        sentence_list.append(review)
        #encode
        encoded_review = tokenizer.encode_plus(
            review,
            max_length=160,
            add_special_tokens=True,
            return_token_type_ids=False,
            pad_to_max_length=True,
            return_attention_mask=True,
            return_tensors='pt',
            truncation=True,
            )
        input_ids = encoded_review['input_ids'].to(device)
        attention_mask = encoded_review['attention_mask'].to(device)
        #output from model
        output = model(input_ids, attention_mask)
        with torch.no_grad():
            probabilities = F.softmax(output, dim=1)
        confidence, predicted_class = torch.max(probabilities, dim=1)
        predicted_class = predicted_class.cpu().item()
        probabilities = probabilities.flatten().cpu().numpy().tolist()
        question_type.append(class_names[predicted_class])

        #update counters
        if class_names[predicted_class] == 'NonPrivate':
            non_priv = non_priv + 1
        else:
            priv = priv + 1
            
        probabilities_list.append(dict(zip(class_names, probabilities)))
    count_list = [non_priv, priv]
    information.set_globeal(question_type,sentence_list, probabilities_list, count_list)

    if josnit == 0:
        return render_template('result.html',
                               predictions = question_type, 
                               sentences = sentence_list, 
                               prob = probabilities_list, 
                               counts = count_list)
    else:
            return jsonify({
                            "Predictions":question_type,
                            "Sentances":sentence_list,
                            "Probabilities":probabilities_list,
                            "Counts":
                            {"NonPrivate":count_list[0],
                            "Private":count_list[1]}
                            })
      
if __name__ == '__main__':
    app.run(host='0.0.0.0')