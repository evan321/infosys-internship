# Privacy Classifier
This service classifies questions by their privacy types ('NonPrivate', 'Private') and produces a distribution of privacy types

# Configurations
```bash
[packages]
Flask 1.1.2
Flask-cors 3.0.8
Jinja2 2.11.2
Pytorch 1.5.1
Transformers 3.0.2
```

# Development
To start the application, run: 
```bash
python /src/app.py
```

# Docker 
To run the docker image, run: 
```bash
docker run -d -p 5000:5000 registry.gitlab.com/stg-et/common-ai-services/privacy-classifer
```

# Model Documentation
Model notebook code is located in ./model.ipynb
<br>
Model python code is located in ./model.py

<br>
[Model Documentation](model_doc.md)

# File Directories
The question answer type classifier outputs the distributions of answer types from all the statements as well as the answer type probabilities for each statement. 

1. ./src/app.py: python file to deploy model Postman json request
2. ./src/Best_model_state.bin : model was trained using google colab’s environment and downloaded to the project
4. ./src/templates/index.html: html page for user to input statement
5. ./src/templates/result.html: html page for resulting sentiment
6. ./src//static/styles.css: css for html appearance

# POST Request Types
For json output request: http://127.0.0.1:5000/privacy-classifier/json

For html output request: http://127.0.0.1:5000/privacy-classifier/text

# Example Request app.py
```bash
curl --location --request POST 'http://127.0.0.1:5000/privacy-classifier/json' \
--data-raw '{ "statement": [
    "How good was the customer service?", 
    "what is your social security?",
    "Have you faced discrimination at work?", 
    "Where are you from?", 
    "What is your employee ID>",
    "Have you experienced harassment at work>",
    "Do you enjoy work?",
    "How was your day?",
    "How impactful was this pandemic?"
     ] }'
```

# Input json file 
```bash
{ "statement": [
    "How good was the customer service?", 
    "what is your social security?",
    "Have you faced discrimination at work?", 
    "Where are you from?", 
    "What is your employee ID>",
    "Have you experienced harassment at work>",
    "Do you enjoy work?",
    "How was your day?",
    "How impactful was this pandemic?"
     ] }
```

# Output json file
```bash
{
    "Counts": {
        "NonPrivate": 4,
        "Private": 5
    },
    "Predictions": [
        "NonPrivate",
        "Private",
        "Private",
        "Private",
        "Private",
        "Private",
        "NonPrivate",
        "NonPrivate",
        "NonPrivate"
    ],
    "Probabilities": [
        {
            "NonPrivate": 0.9994472861289978,
            "Private": 0.0005526843015104532
        },
        {
            "NonPrivate": 0.00016644109564367682,
            "Private": 0.9998335838317871
        },
        {
            "NonPrivate": 0.0005430624005384743,
            "Private": 0.9994569420814514
        },
        {
            "NonPrivate": 0.004730247892439365,
            "Private": 0.9952697157859802
        },
        {
            "NonPrivate": 0.00024125004711095244,
            "Private": 0.999758780002594
        },
        {
            "NonPrivate": 0.0009958940790966153,
            "Private": 0.9990041851997375
        },
        {
            "NonPrivate": 0.9999078512191772,
            "Private": 9.214973397320136e-05
        },
        {
            "NonPrivate": 0.9986674785614014,
            "Private": 0.001332579762674868
        },
        {
            "NonPrivate": 0.9992615580558777,
            "Private": 0.0007384229102171957
        }
    ],
    "Sentances": [
        "How good was the customer service?",
        "what is your social security?",
        "Have you faced discrimination at work?",
        "Where are you from?",
        "What is your employee ID>",
        "Have you experienced harassment at work>",
        "Do you enjoy work?",
        "How was your day?",
        "How impactful was this pandemic?"
    ]
}
```

# Postman Documentation



# References 
https://towardsdatascience.com/deploying-bert-using-kubernetes-6ddca23caec5

https://www.curiousily.com/posts/deploy-bert-for-sentiment-analysis-as-rest-api-using-pytorch-transformers-by-hugging-face-and-fastapi/

https://www.curiousily.com/posts/sentiment-analysis-with-bert-and-hugging-face-using-pytorch-and-python/

https://ai.googleblog.com/2018/11/open-sourcing-bert-state-of-art-pre.html

https://medium.com/@doedotdev/docker-flask-a-simple-tutorial-bbcb2f4110b5?fbclid=IwAR0fdSncZZgIlmnUcQREdesHUSmghT3ZLq6Rx1ciMPsxaHopD3n7_HlMxmo
