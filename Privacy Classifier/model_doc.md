# Model 
This documents model development and configurations. See ./model.py for code.

# Data Set: 
See ./training_set.csv 
<br>
See ./testing_set.csv
<br>

The model utilizes Infosys dataset on privacy as well as research on information considered as "private" according to Personally Identifiable Information (PII) online rules. 
1. 1068 non-private questions
2. 412 private questions
<br>

# Data Preprocessing: 
BERT (Bidirectional Encoder Representations from Transformers) is used in order to pre-train the contextualized word embeddings. BERT applies a bidirectional training of the Transformers attention model. To use BERT, the model is utilizing Pytorch and  the Transformers Library by HuggingFace.

Three encoding requirements are needed to use BERT: 
1. Special tokens to delineate separate sentences
2. Introduce padding to ensure constant length of review
3. Create attention mask (pad and real tokens)
The pre-trained Bert Tokenizer model has a function that encodes the data ( tokenizer.encode_plus() )  

# Model Development: 
The following model was created using code from Venelin Valkov ( see references ). 

The model uses a neural network classifier based on the BERT model, a dropout layer ( to avoid overfitting), and a fully-connected layer. The BERT model utilizes fixed sequence length. Based on distribution of review lengths, the max length chosen is 160. A softmax function is used to predict the probabilities of each sentiment. Finally, to correct for weight decay, the AdamW optimizer is used. 

The fine-tuning parameters recommended by the BERT developers are as follows: 

Batch size: 16, 32 ( model used 16 ) 
Learning rate (Adam): 5e-5, 3e-5, 2e-5 (  model used 2e-5 ) 
Number of epochs: 2, 3, 4 ( model used 10 ) 
10 epoch was used because it produced the greatest training accuracy results. 
The model produced a testing set accuracy of 0.883248730964467
The confusion matrix is attached in model_confusion_matrix.png. 


# Model Deployment: 
The model is deployed using the Flask web framework. The Flask CORS library implements Cross Origin Resource Sharing (CORS) capabilities.  The user may input statements via a json file or text box. 