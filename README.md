# Infosys Internship
In developing customer-centric products that interface with people, analyzing natural language is not only important but also a recurring task. An example of this can include analyzing customer reviews on a product. Artificial intelligence is the most pervasive tool that products use to understand natural language. Therefore, standardizing high-quality NLP AI services is useful in understanding customer information and streamlining larger, wide-spread problems. The classifiers will be deployed using the Flask web framework. 
The following three AI classifiers will be developed using the following modulated process:
​
# Sentiment Classifier
Given a text, the model will output the overall sentiment (positive, neutral, negative)

# Question-Answer Type Classifier
Given a question, the model will output the answer type (small text, small text- location, small text- person, long text, multiple choice, date and time, yes/no, numeric)

# Privacy Classifier
 Given a question, the model will output the question privacy level (nonprivate, private)